#Install OpenJDK and JRE
sudo apt-get install default-jdk -y
sudo apt-get install openjdk-8-jre -y
sudo apt-get update
#Add the Elasticsearch APT repo in your package source list
echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-5.x.list
#signing_key and update repository
sudo apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv D88E42B4
sudo apt-get install apt-transport-https -y
#install elasticsearch
sudo apt-get update -y
sudo apt-get install elasticsearch -y

#AUTHENTICATION AND AUTHORIZATION
sudo cd /usr/share/elasticsearch
sudo bin/elasticsearch-plugin install x-pack
cluster.name: sample-cluster ==>/etc/elasticsearch/elasticsearch.yml
node.name: sample-node ==>/etc/elasticsearch/elasticsearch.yml
http.port: 9200-9300 >>/etc/elasticsearch/elasticsearch.yml
action.auto_create_index: .security,.monitoring*,.watches,.triggered_watches,.watcher-history*,.ml* >>/etc/elasticsearch/elasticsearch.yml
sed -i 's/0.0.0.0/127.0.0.1/g' /etc/elasticsearch/elasticsearch.yml


#Optional: Create a certificate authority for your Elasticsearch cluster
elasticsearch-certutil ca
elasticsearch-certutil cert --ca elastic-stack-ca.p12

xpack.security.transport.ssl.enabled: true >>/etc/elasticsearch/elasticsearch.yml
xpack.security.transport.ssl.verification_mode: certificate >>/etc/elasticsearch/elasticsearch.yml
xpack.security.transport.ssl.keystore.path: certs/elastic-certificates.p12 >>/etc/elasticsearch/elasticsearch.yml
xpack.security.transport.ssl.truststore.path: certs/elastic-certificates.p12 >>/etc/elasticsearch/elasticsearch.yml


sudo service elasticsearch restart

#You must reset the default passwords for all built-in users
curl -XPUT -u elastic 'localhost:9200/_xpack/security/user/elastic/_password' -H "Content-Type: application/json" -d '
{  
 "password": "linuxPoint123"
}

